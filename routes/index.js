var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});
router.get('/funcionamiento', function(req, res, next){
    res.render('funcionamiento');
});
router.get('/info-psicologos', function(req, res, next){
    res.render('info-psicologos');
});
router.get('/angular', function(req, res, next) {
    res.render('angular');
});

module.exports = router;

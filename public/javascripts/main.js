
var app = angular.module('App', ['ui.bootstrap', 'ngAnimate']);

app.controller('Collapse', function($scope){
    $scope.isCollaped = false;
});
app.controller('CarouselPacientes', function($scope){
    $scope.myInterval = 8000;
    var slides = $scope.slides = [
        {
            image: '/images/videoconference.png',
            title: 'Sin desplazamientos',
            text: "Reduce el <b>tiempo</b> y el <b>costo</b> de <b>transporte</b> hacia el consultorio, la sesión será <b>donde estés</b>. Las dificultades físicas <b>ya no son un impedimento</b>.<br><br><b>Accede ahora</b> desde tu celular, tablet, laptop, computadora de escritorio. <b>¡Tú eliges!</b>"
        },
        {
            image: '/images/videoconference.png',
            title: 'Sin salas de espera, nadie tiene por qué enterarse',
            text: '¿Fuiste al <b>psicólogo</b> y te encontraste con <b>alguien conocido</b> en la sala de espera? Creemos que <b>no hay que avergonzarse</b> por ir al psicólogo, pero sabemos que lleva un tiempo acostumbrarse.<br><br>Siéntate cómodamente y <b>accede</b>, <b>¡Sin que nadie se entere!</b>'
        },
        {
            image: '/images/videoconference.png',
            title: 'Elige a cualquier psicólogo, sin importar dónde se encuentre',
            text: 'Si no hay muchos psicólogos en tu ciudad, o si <b>no te sientes cómodo</b> con ninguno de ellos, simplemente elige un psicólogo de <b>cualquier parte del Perú</b>, recuerda que las sesiones serán <b>virtuales</b>, y no necesitas trasladarte al consultorio.<br><br><b>¡Elige al profesional que mejor se adapte a tus necesidades!</b>'
        },
        {
            image: '/images/videoconference.png',
            title: 'Llamadas de voz, chat, videollamadas, como te sientas más cómodo',
            text: 'Nunca ha sido fácil mirar a un desconocido a los ojos y contarle tus problemas más íntimos, por ello te ofrecemos la posibilidad de <b>comunicarte</b> con el psicólogo de la forma que <b>tú prefieras</b>. <br><br>¡<b>Separa tu cita</b> y comienza a vivir esta <b>experiencia única</b>!'
        }
    ];
});
app.controller('CarouselPsicologo', function($scope){
    $scope.myInterval = 8000;
    var slides = $scope.slides = [
        {
            image: '/images/videoconference.png',
            title: 'Expande tus servicios, no tengas fronteras',
            text: "Reduce el <b>tiempo</b> y el <b>costo</b> de <b>transporte</b> hacia el consultorio, la sesión será <b>donde estés</b>. Las dificultades físicas <b>ya no son un impedimento</b>.<br><br><b>Accede ahora</b> desde tu celular, tablet, laptop, computadora de escritorio. <b>¡Tú eliges!</b>"
        },
        {
            image: '/images/videoconference.png',
            title: 'Ya no pagues por el alquiler del consultorio y costes relacionados',
            text: '¿Fuiste al <b>psicólogo</b> y te encontraste con <b>alguien conocido</b> en la sala de espera? Creemos que <b>no hay que avergonzarse</b> por ir al psicólogo, pero sabemos que lleva un tiempo acostumbrarse.<br><br>Siéntate cómodamente y <b>accede</b>, <b>¡Sin que nadie se entere!</b>'
        },
        {
            image: '/images/videoconference.png',
            title: 'Te ofrecemos conferencias con psicólogos de renombre',
            text: 'Si no hay muchos psicólogos en tu ciudad, o si <b>no te sientes cómodo</b> con ninguno de ellos, simplemente elige un psicólogo de <b>cualquier parte del Perú</b>, recuerda que las sesiones serán <b>virtuales</b>, y no necesitas trasladarte al consultorio.<br><br><b>¡Elige al profesional que mejor se adapte a tus necesidades!</b>'
        },
        {
            image: '/images/videoconference.png',
            title: 'Atiende desde casa, tú estableces los horarios',
            text: 'Nunca ha sido fácil mirar a un desconocido a los ojos y contarle tus problemas más íntimos, por ello te ofrecemos la posibilidad de <b>comunicarte</b> con el psicólogo de la forma que <b>tú prefieras</b>. <br><br>¡<b>Separa tu cita</b> y comienza a vivir esta <b>experiencia única</b>!'
        }
    ];
});

app.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

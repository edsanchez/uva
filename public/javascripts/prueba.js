angular.module('finance', [])
    .factory('currencyConverter', function(){
        var currencies = ["CNY", "USD", "EUR"];
        var usdToForeignRates = {
            USD: 1,
            EUR: 0.74,
            CNY: 6.09
        };
        var convert = function (amount, inCurr, outCurr) {
            return amount * usdToForeignRates[outCurr] / usdToForeignRates[inCurr];
        };

        return {
            currencies: currencies,
            convert: convert
        };
    });
var app = angular.module('Prueba', ['finance'])
    .controller('InvoiceController', ['currencyConverter', function(currencyConverter){
        this.qty = 1;
        this.cost = 1;
        this.inCurr = "EUR";
        this.currencies = currencyConverter.currencies;

        this.total = function(out){
            return currencyConverter.convert(this.qty * this.cost, this.inCurr, out);
        };
        this.pay = function pay() {
            window.alert("Thanks!");
        };
    }]);

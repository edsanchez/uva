angular.module('finance', [])
    .factory('currencyConverter', function(){
        var currencies = ["CNY", "USD", "EUR"];
        var usdToForeignRates = {
            USD: 1,
            EUR: 0.74,
            CNY: 6.09
        };
        var convert = function (amount, inCurr, outCurr) {
            return amount * this.usdToForeignRates[outCurr] / this.usdToForeignRates[inCurr];
        };

        return {
            currencies: currencies,
            convert: convert
        };
    });
